Праивла работы с LiquiBase

changesets - вести в xml формате и групировать по сущностям и релизам

в init директории необходимы тольо те changeset которые выстпают для первой иницилизации БД. Создание таблиц, заполенение справочников данными.

Правила именование файлов(changelog)

[**DATA**(yyyy-mm-dd)].[**PREFIX**].[**ENTITY**]_[**FIELD_NAME**].xml

[**DATA**(yyyy-mm-dd)] - опциональный параметр, используется только в папках для релизов, в init директории все changesets без даты.
Формат даты yyyy-mm-dd

- [**PREFIX**] - обьазталеьный параметр, покаывающий какое действие выполняется над сущностью
    - **C** - create
    - **I** - insert
    - **D** - delete 
    - **C_IDX** - create index(IDX)
    - **A** - alter
    - **PJ00000** - коде дефекта PJ - имя проекта, 00000 - номер задачи

- [**ENTITY**] - имя сущности для изменения - таблица, индекс, процедура(может указыватся схема)
- [**FIELD_NAME**] - опциональный параметр

Примеры:
- **2020.12.04.C_ORDERS.xml** 
- **2020.12.04.A_ORDERS_number.xml**

Примеры:

Preconditions: 
- Поск типа поля  - **select data_type from information_schema.columns where table_name = '[table_name]' and column_name = '[column_name]';**
- Is not null  - **select is_nullable from information_schema.columns where table_name = '[table_name]' and column_name = '[column_name]';**